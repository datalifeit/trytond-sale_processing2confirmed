# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import invoice


def register():
    Pool.register(
        sale.Sale,
        module='sale_processing2confirmed', type_='model')
    Pool.register(
        invoice.Sale,
        invoice.Invoice,
        module='sale_processing2confirmed', type_='model',
        depends=['account_invoice_posted2draft'])
