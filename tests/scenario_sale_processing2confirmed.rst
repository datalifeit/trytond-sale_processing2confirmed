=====================================
Scenario Sale Processing to Confirmed
=====================================

Imports::

    >>> import datetime
    >>> import os
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import config, Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.exceptions import UserError
    >>> today = datetime.date.today()

Install sale_processing2confirmed::

    >>> config = activate_modules(['sale_processing2confirmed', 'stock_move_done2cancel'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.customer_tax_rule = None
    >>> customer.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> kilo, = ProductUom.find([('name', '=', 'Kilogram')])

    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')

    >>> product1 = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'PROD1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price = Decimal('5.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1.template = template
    >>> product1.code = 'PROD1'
    >>> product1.save()

    >>> product2 = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'PROD2'
    >>> template.default_uom = gram
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price = Decimal('5.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2.template = template
    >>> product2.code = 'PROD2'
    >>> product2.save()

    >>> product3 = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'PROD3'
    >>> template.default_uom = kilo
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price = Decimal('5.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product3.template = template
    >>> product3.code = 'PROD3'
    >>> product3.save()

    >>> service = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service.template = template
    >>> service.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product1)
    >>> inventory_line.quantity = 100.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory_line = inventory.lines.new(product=product2)
    >>> inventory_line.quantity = 50.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory_line = inventory.lines.new(product=product3)
    >>> inventory_line.quantity = 20.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Create a sale::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product2
    >>> sale_line.quantity = 20.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product3
    >>> sale_line.quantity = 10.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = service
    >>> sale_line.quantity = 1
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.click('confirm')

Duplicate Sale::

    >>> posted_invoice_sale, = Sale.duplicate([sale],
    ...     {'description' : 'Posted invoice sale'})
    >>> posted_shipment_sale, = Sale.duplicate([sale],
    ...    {'description' : 'Posted shipment sale'})

Process sale::

    >>> sale.click('process')
    >>> sale.state
    'processing'
    >>> len(sale.shipments), len(sale.shipment_returns), len(sale.invoices)
    (1, 0, 1)

Go back to confirmed on original sale::

    >>> sale.state
    'processing'
    >>> not sale.shipment_number_cache
    True
    >>> number = sale.shipments[0].number
    >>> sale.click('unprocess')
    >>> sale.state
    'draft'
    >>> sale.shipment_state
    'none'
    >>> sale.invoice_state
    'none'
    >>> len(sale.shipments), len(sale.shipment_returns), len(sale.invoices)
    (0, 0, 0)
    >>> sale.shipment_number_cache == number
    True

Process again and check shipment number::

    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.shipments[0].number == number
    True

Check reload taxes::

    >>> Invoice = Model.get('account.invoice')

    >>> invoice, = Invoice.find([])
    >>> invoice_line = invoice.lines.new()
    >>> invoice_line.quantity = 1
    >>> invoice_line.unit_price = Decimal('100.0')
    >>> invoice_line.account = revenue
    >>> invoice_line.product = product1
    >>> invoice.save()

    >>> invoice.untaxed_amount
    Decimal('450.00')
    >>> invoice.tax_amount
    Decimal('45.00')
    >>> invoice.total_amount
    Decimal('495.00')

    >>> sale.click('unprocess')
    >>> invoice.reload()
    >>> len(invoice.lines)
    1
    >>> invoice.untaxed_amount
    Decimal('100.00')
    >>> invoice.tax_amount
    Decimal('10.00')
    >>> invoice.total_amount
    Decimal('110.00')

Process posted invoice sales::

    >>> posted_invoice_sale.click('quote')
    >>> posted_invoice_sale.click('confirm')
    >>> posted_invoice_sale.click('process')
    >>> invoices = [invoice for invoice in posted_invoice_sale.invoices]

Post invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> for invoice in invoices:
    ...     invoice.click('post')

Draft invoice sale::

    >>> try:
    ...     posted_invoice_sale.click('unprocess')
    ... except UserError:
    ...     pass
    >>> posted_invoice_sale.state
    'processing'

Validate Shipments::

    >>> posted_shipment_sale.click('quote')
    >>> posted_shipment_sale.click('confirm')
    >>> posted_shipment_sale.click('process')
    >>> shipment, = posted_shipment_sale.shipments
    >>> shipment.click('assign_try')
    >>> shipment.click('pick')
    >>> shipment.click('pack')
    >>> shipment.click('done')

Draft shipment sale::

    >>> try:
    ...     posted_shipment_sale.click('unprocess')
    ... except:
    ...     pass
    >>> posted_shipment_sale.state
    'draft'
