================================
Scenario Invoice Posted to Draft
================================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from decimal import Decimal
    >>> from proteus import config, Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts, create_fiscalyear
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences


Install sale_processing2confirmed::

    >>> config = activate_modules(['sale_processing2confirmed',
    ...     'account_invoice_posted2draft'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']


Create parties::

    >>> Party = Model.get('party.party')
    >>> PartyIdentifier = Model.get('party.identifier')

    >>> customer = Party(name='Customer')
    >>> customer.customer_tax_rule = None
    >>> customer.save()

    >>> tax_identifier = PartyIdentifier()
    >>> tax_identifier.code = '92079673R'
    >>> tax_identifier.type = 'es_dni'
    >>> tax_identifier.party = customer
    >>> tax_identifier.save()

    >>> customer2 = Party(name='Customer 2')
    >>> customer2.customer_tax_rule = None
    >>> customer2.save()

    >>> tax_identifier = PartyIdentifier()
    >>> tax_identifier.code = '33677596F'
    >>> tax_identifier.type = 'es_dni'
    >>> tax_identifier.party = customer2
    >>> tax_identifier.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_revenue = revenue
    >>> account_category.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])

    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'PROD1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price = Decimal('5.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products


Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([('code', '=', 'STO')])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.quantity = 1000.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm')


Create a sale::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> Invoice = Model.get('account.invoice')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 50.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.state
    'processing'
    >>> len(Invoice.find([]))
    1
    >>> invoice, = sale.invoices
    >>> invoice.click('post')
    >>> invoice.number
    '1'
    >>> invoice.click('draft')
    >>> invoice.number != None
    True
    >>> sale.invoice_cache == None
    True
    >>> len(invoice.lines)
    1
    >>> sale.click('unprocess')
    >>> len(Invoice.find([]))
    1
    >>> invoice = Invoice(invoice.id)
    >>> sale.invoice_cache == invoice
    True
    >>> not sale.invoices
    True
    >>> len(invoice.lines)
    0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.state
    'processing'
    >>> not sale.invoice_cache
    True
    >>> len(Invoice.find([]))
    1
    >>> invoice = Invoice(invoice.id)
    >>> len(invoice.lines)
    1
    >>> invoice.lines[0].origin == sale.lines[0]
    True


Create another invoices::

    >>> invoice2 = Invoice()
    >>> invoice2.party = customer
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 40
    >>> line.unit_price = Decimal('5')
    >>> invoice2.save()

    >>> invoice2.click('post')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot post Invoice because exists numbered invoice "1" in draft State. You must post it first. - 

    >>> invoice.click('post')
    >>> invoice.number
    '1'
    >>> invoice2.click('post')
    >>> invoice2.number
    '2'

Change header with wizzard::

    >>> invoice.click('draft')
    >>> sale.click('unprocess')
    >>> modify_header = Wizard('sale.modify_header', [sale])
    >>> modify_header.form.invoice_party = customer2
    >>> modify_header.execute('modify')

    >>> sale.click('quote')
    >>> sale.click('confirm')

Check invoice::

    >>> invoice, = sale.invoices
    >>> invoice.party == customer2
    True
    >>> invoice.party_tax_identifier == customer2.tax_identifier
    True
    >>> invoice.invoice_address == customer2.addresses[0]
    True

