# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class SaleProcessing2ConfirmedTestCase(ModuleTestCase):
    'Test Sale Processing To Confirmed module'
    module = 'sale_processing2confirmed'
    extras = ['stock_move_done2cancel', 'account_invoice_posted2draft']


del ModuleTestCase
